const express = require("express");
const redis  = require("redis")

const app = express();
const client = redis.createClient({
    host: "redis-server",
    port: 6379
});

client.on('error', (err) => {
    console.log('Error occured while connecting or accessing redis server');
});

client.set('counter', 0);
app.get("/", (req, res) => {
    client.get('counter', (err, counter) => {
        res.send('Page Validator Counter : ' + counter);
        client.set('counter', parseInt(counter) + 1);
    });
});

app.listen(4001, () => {
    console.log('Listening on port 4001');
})